﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManager : GameManagerBehavior
{
    public enum State
    {
        Warmup,
        InRound,
        PostPlant,
        PostRound,
        Buy
    }

    public enum Team 
    {
        None,
        Defend,
        Attack
    }

    private Dictionary<uint, Team> playerList = new Dictionary<uint, Team>();

    public Text gameText;

    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        networkObject.State = (int)State.Warmup;

        //Change to more changeable values, for now it will be hard coded
        networkObject.PostRoundTime = 10;
        networkObject.RoundTime = 60;
        networkObject.BuyTime = 20;
        networkObject.DefuseTime = 40;
        networkObject.MaxRounds = 15;

        if (NetworkManager.Instance.IsServer)
        {
            NetworkManager.Instance.Networker.playerConnected += PlayerConnected;
            NetworkManager.Instance.Networker.playerDisconnected += PlayerDisconnected;
            if (!Application.isBatchMode)
            {
                playerList.Add(0, Team.Attack);
            }
        }
    }

    private void UpdateUI<T>(T field, ulong timestep)
    {
        Debug.Log("UpdateUI");
        
    }

    private void PlayerConnected(NetworkingPlayer player, NetWorker sender)
    {
        int defenders = 0;
        int attackers = 0;
        foreach (KeyValuePair<uint, Team> kvp in playerList)
        {
            if (kvp.Value == Team.Attack)
            {
                attackers++;
            }
            else if(kvp.Value == Team.Defend)
            {
                defenders++;
            }
        }

        if (defenders >= attackers)
        {
            playerList.Add(player.NetworkId, Team.Attack);
        }
        else if(defenders < attackers)
        {
            playerList.Add(player.NetworkId, Team.Defend);
        }

        foreach (KeyValuePair<uint, Team> kvp in playerList)
        {
            Debug.Log(kvp.Key +  ":" + kvp.Value);
        }

        networkObject.SendRpc(RPC_UPDATE_PLAYER_LIST, Receivers.AllBuffered, PlayerListToByteArray(playerList));

        if (attackers+defenders >= 2)
        {
            SetState(State.Buy);
        }
    }

    private void PlayerDisconnected(NetworkingPlayer player, NetWorker sender)
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (networkObject.IsServer)
        {
            UpdateTime();
            if (networkObject.State == (int)State.Warmup)
            {
                if (NetworkManager.Instance.Networker.Players.Count >= 2)
                {
                    networkObject.State = (int)State.Buy;
                    networkObject.RoundTimer = 0;
                }
            }
            else if (networkObject.State == (int)State.Buy)
            {
                if (networkObject.RoundTimer > networkObject.BuyTime)
                {
                    networkObject.State = (int)State.InRound;
                    networkObject.RoundTimer = 0;
                }
            }
            else if (networkObject.State == (int)State.InRound)
            {
                if (networkObject.RoundTimer > networkObject.RoundTime)
                {
                    networkObject.State = (int)State.PostRound;
                    networkObject.RoundTimer = 0;
                }
            }
            else if (networkObject.State == (int)State.PostPlant)
            {
                if (networkObject.RoundTimer > networkObject.DefuseTime)
                {
                    networkObject.State = (int)State.Buy;
                    networkObject.RoundTimer = 0;
                    networkObject.Round++;
                }
            }
            else if (networkObject.State == (int)State.PostRound)
            {
                if (networkObject.RoundTimer > networkObject.PostRoundTime)
                {
                    networkObject.State = (int)State.Buy;
                    networkObject.RoundTimer = 0;
                    networkObject.Round++;
                }
            }
        }

        string state = "";
        if (networkObject.State == (int)State.Warmup)
        {
            state = "Warmup";
        }
        else if (networkObject.State == (int)State.Buy)
        {
            state = "Buy";
        }
        else if (networkObject.State == (int)State.InRound)
        {
            state = "InRound";
        }
        else if (networkObject.State == (int)State.PostPlant)
        {
            state = "Post Plant";
        }
        else if (networkObject.State == (int)State.PostRound)
        {
            state = "PostRound";
        }
        gameText.text = "Round: " + networkObject.Round + "/" + networkObject.MaxRounds + "\n"
            + "Time: " + networkObject.RoundTimer + "\n"
            + "Round State: " + state;
    }

    public void SetState(State state)
    {
        if (networkObject.IsServer)
        {
            networkObject.State = (int)state;
            networkObject.RoundTimer = 0;
        }
    }

    void PlayersToSpawns()
    {
        foreach (KeyValuePair<uint, Team> kvp in playerList)
        {
            if (kvp.Value == Team.Attack)
            {

            }
            else if(kvp.Value == Team.Defend)
            {

            }
        }
    }


    void UpdateTime()
    {
        Debug.Log("UpdateTime");
        if (networkObject.IsServer)
        {
            networkObject.RoundTimer += Time.deltaTime;
        }
    }

    public Team GetPlayerTeam(uint playerID)
    {
        foreach (KeyValuePair<uint, Team> kvp in playerList)
        {
            Debug.Log(kvp.Key + ":" + kvp.Value);
        }

        Team team;
        playerList.TryGetValue(playerID, out team);
        return team;
    }

    public override void UpdatePlayerList(RpcArgs args)
    {
        if (!NetworkManager.Instance.IsServer)
        {
            byte[] listData = args.GetNext<byte[]>();

            playerList = ByteArrayToPlayerList(listData);

            foreach (KeyValuePair<uint, Team> kvp in playerList)
            {
                Debug.Log(kvp.Key + ":" + kvp.Value);
            }
        }      
    }

    public byte[] PlayerListToByteArray(Dictionary<uint, Team> dictionary)
    {
        var binFormatter = new BinaryFormatter();
        var mStream = new MemoryStream();
        binFormatter.Serialize(mStream, dictionary);

        //This gives you the byte array.
        return mStream.ToArray();
    }

    public Dictionary<uint, Team> ByteArrayToPlayerList(byte[] arrBytes)
    {
        var mStream = new MemoryStream();
        var binFormatter = new BinaryFormatter();

        // Where 'objectBytes' is your byte array.
        mStream.Write(arrBytes, 0, arrBytes.Length);
        mStream.Position = 0;

        return binFormatter.Deserialize(mStream) as Dictionary<uint, Team>;
    }
}
