using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0,0,0,0,0,0,0,0,0]")]
	public partial class GameManagerNetworkObject : NetworkObject
	{
		public const int IDENTITY = 10;

		private byte[] _dirtyFields = new byte[2];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private int _Round;
		public event FieldEvent<int> RoundChanged;
		public Interpolated<int> RoundInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int Round
		{
			get { return _Round; }
			set
			{
				// Don't do anything if the value is the same
				if (_Round == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_Round = value;
				hasDirtyFields = true;
			}
		}

		public void SetRoundDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_Round(ulong timestep)
		{
			if (RoundChanged != null) RoundChanged(_Round, timestep);
			if (fieldAltered != null) fieldAltered("Round", _Round, timestep);
		}
		[ForgeGeneratedField]
		private int _MaxRounds;
		public event FieldEvent<int> MaxRoundsChanged;
		public Interpolated<int> MaxRoundsInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int MaxRounds
		{
			get { return _MaxRounds; }
			set
			{
				// Don't do anything if the value is the same
				if (_MaxRounds == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_MaxRounds = value;
				hasDirtyFields = true;
			}
		}

		public void SetMaxRoundsDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_MaxRounds(ulong timestep)
		{
			if (MaxRoundsChanged != null) MaxRoundsChanged(_MaxRounds, timestep);
			if (fieldAltered != null) fieldAltered("MaxRounds", _MaxRounds, timestep);
		}
		[ForgeGeneratedField]
		private float _RoundTime;
		public event FieldEvent<float> RoundTimeChanged;
		public InterpolateFloat RoundTimeInterpolation = new InterpolateFloat() { LerpT = 0f, Enabled = false };
		public float RoundTime
		{
			get { return _RoundTime; }
			set
			{
				// Don't do anything if the value is the same
				if (_RoundTime == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x4;
				_RoundTime = value;
				hasDirtyFields = true;
			}
		}

		public void SetRoundTimeDirty()
		{
			_dirtyFields[0] |= 0x4;
			hasDirtyFields = true;
		}

		private void RunChange_RoundTime(ulong timestep)
		{
			if (RoundTimeChanged != null) RoundTimeChanged(_RoundTime, timestep);
			if (fieldAltered != null) fieldAltered("RoundTime", _RoundTime, timestep);
		}
		[ForgeGeneratedField]
		private float _RoundTimer;
		public event FieldEvent<float> RoundTimerChanged;
		public InterpolateFloat RoundTimerInterpolation = new InterpolateFloat() { LerpT = 0f, Enabled = false };
		public float RoundTimer
		{
			get { return _RoundTimer; }
			set
			{
				// Don't do anything if the value is the same
				if (_RoundTimer == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x8;
				_RoundTimer = value;
				hasDirtyFields = true;
			}
		}

		public void SetRoundTimerDirty()
		{
			_dirtyFields[0] |= 0x8;
			hasDirtyFields = true;
		}

		private void RunChange_RoundTimer(ulong timestep)
		{
			if (RoundTimerChanged != null) RoundTimerChanged(_RoundTimer, timestep);
			if (fieldAltered != null) fieldAltered("RoundTimer", _RoundTimer, timestep);
		}
		[ForgeGeneratedField]
		private float _BuyTime;
		public event FieldEvent<float> BuyTimeChanged;
		public InterpolateFloat BuyTimeInterpolation = new InterpolateFloat() { LerpT = 0f, Enabled = false };
		public float BuyTime
		{
			get { return _BuyTime; }
			set
			{
				// Don't do anything if the value is the same
				if (_BuyTime == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x10;
				_BuyTime = value;
				hasDirtyFields = true;
			}
		}

		public void SetBuyTimeDirty()
		{
			_dirtyFields[0] |= 0x10;
			hasDirtyFields = true;
		}

		private void RunChange_BuyTime(ulong timestep)
		{
			if (BuyTimeChanged != null) BuyTimeChanged(_BuyTime, timestep);
			if (fieldAltered != null) fieldAltered("BuyTime", _BuyTime, timestep);
		}
		[ForgeGeneratedField]
		private byte _PostRoundTime;
		public event FieldEvent<byte> PostRoundTimeChanged;
		public Interpolated<byte> PostRoundTimeInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte PostRoundTime
		{
			get { return _PostRoundTime; }
			set
			{
				// Don't do anything if the value is the same
				if (_PostRoundTime == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x20;
				_PostRoundTime = value;
				hasDirtyFields = true;
			}
		}

		public void SetPostRoundTimeDirty()
		{
			_dirtyFields[0] |= 0x20;
			hasDirtyFields = true;
		}

		private void RunChange_PostRoundTime(ulong timestep)
		{
			if (PostRoundTimeChanged != null) PostRoundTimeChanged(_PostRoundTime, timestep);
			if (fieldAltered != null) fieldAltered("PostRoundTime", _PostRoundTime, timestep);
		}
		[ForgeGeneratedField]
		private float _DefuseTime;
		public event FieldEvent<float> DefuseTimeChanged;
		public InterpolateFloat DefuseTimeInterpolation = new InterpolateFloat() { LerpT = 0f, Enabled = false };
		public float DefuseTime
		{
			get { return _DefuseTime; }
			set
			{
				// Don't do anything if the value is the same
				if (_DefuseTime == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x40;
				_DefuseTime = value;
				hasDirtyFields = true;
			}
		}

		public void SetDefuseTimeDirty()
		{
			_dirtyFields[0] |= 0x40;
			hasDirtyFields = true;
		}

		private void RunChange_DefuseTime(ulong timestep)
		{
			if (DefuseTimeChanged != null) DefuseTimeChanged(_DefuseTime, timestep);
			if (fieldAltered != null) fieldAltered("DefuseTime", _DefuseTime, timestep);
		}
		[ForgeGeneratedField]
		private bool _BombPlanted;
		public event FieldEvent<bool> BombPlantedChanged;
		public Interpolated<bool> BombPlantedInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool BombPlanted
		{
			get { return _BombPlanted; }
			set
			{
				// Don't do anything if the value is the same
				if (_BombPlanted == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x80;
				_BombPlanted = value;
				hasDirtyFields = true;
			}
		}

		public void SetBombPlantedDirty()
		{
			_dirtyFields[0] |= 0x80;
			hasDirtyFields = true;
		}

		private void RunChange_BombPlanted(ulong timestep)
		{
			if (BombPlantedChanged != null) BombPlantedChanged(_BombPlanted, timestep);
			if (fieldAltered != null) fieldAltered("BombPlanted", _BombPlanted, timestep);
		}
		[ForgeGeneratedField]
		private int _State;
		public event FieldEvent<int> StateChanged;
		public Interpolated<int> StateInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int State
		{
			get { return _State; }
			set
			{
				// Don't do anything if the value is the same
				if (_State == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[1] |= 0x1;
				_State = value;
				hasDirtyFields = true;
			}
		}

		public void SetStateDirty()
		{
			_dirtyFields[1] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_State(ulong timestep)
		{
			if (StateChanged != null) StateChanged(_State, timestep);
			if (fieldAltered != null) fieldAltered("State", _State, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			RoundInterpolation.current = RoundInterpolation.target;
			MaxRoundsInterpolation.current = MaxRoundsInterpolation.target;
			RoundTimeInterpolation.current = RoundTimeInterpolation.target;
			RoundTimerInterpolation.current = RoundTimerInterpolation.target;
			BuyTimeInterpolation.current = BuyTimeInterpolation.target;
			PostRoundTimeInterpolation.current = PostRoundTimeInterpolation.target;
			DefuseTimeInterpolation.current = DefuseTimeInterpolation.target;
			BombPlantedInterpolation.current = BombPlantedInterpolation.target;
			StateInterpolation.current = StateInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _Round);
			UnityObjectMapper.Instance.MapBytes(data, _MaxRounds);
			UnityObjectMapper.Instance.MapBytes(data, _RoundTime);
			UnityObjectMapper.Instance.MapBytes(data, _RoundTimer);
			UnityObjectMapper.Instance.MapBytes(data, _BuyTime);
			UnityObjectMapper.Instance.MapBytes(data, _PostRoundTime);
			UnityObjectMapper.Instance.MapBytes(data, _DefuseTime);
			UnityObjectMapper.Instance.MapBytes(data, _BombPlanted);
			UnityObjectMapper.Instance.MapBytes(data, _State);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_Round = UnityObjectMapper.Instance.Map<int>(payload);
			RoundInterpolation.current = _Round;
			RoundInterpolation.target = _Round;
			RunChange_Round(timestep);
			_MaxRounds = UnityObjectMapper.Instance.Map<int>(payload);
			MaxRoundsInterpolation.current = _MaxRounds;
			MaxRoundsInterpolation.target = _MaxRounds;
			RunChange_MaxRounds(timestep);
			_RoundTime = UnityObjectMapper.Instance.Map<float>(payload);
			RoundTimeInterpolation.current = _RoundTime;
			RoundTimeInterpolation.target = _RoundTime;
			RunChange_RoundTime(timestep);
			_RoundTimer = UnityObjectMapper.Instance.Map<float>(payload);
			RoundTimerInterpolation.current = _RoundTimer;
			RoundTimerInterpolation.target = _RoundTimer;
			RunChange_RoundTimer(timestep);
			_BuyTime = UnityObjectMapper.Instance.Map<float>(payload);
			BuyTimeInterpolation.current = _BuyTime;
			BuyTimeInterpolation.target = _BuyTime;
			RunChange_BuyTime(timestep);
			_PostRoundTime = UnityObjectMapper.Instance.Map<byte>(payload);
			PostRoundTimeInterpolation.current = _PostRoundTime;
			PostRoundTimeInterpolation.target = _PostRoundTime;
			RunChange_PostRoundTime(timestep);
			_DefuseTime = UnityObjectMapper.Instance.Map<float>(payload);
			DefuseTimeInterpolation.current = _DefuseTime;
			DefuseTimeInterpolation.target = _DefuseTime;
			RunChange_DefuseTime(timestep);
			_BombPlanted = UnityObjectMapper.Instance.Map<bool>(payload);
			BombPlantedInterpolation.current = _BombPlanted;
			BombPlantedInterpolation.target = _BombPlanted;
			RunChange_BombPlanted(timestep);
			_State = UnityObjectMapper.Instance.Map<int>(payload);
			StateInterpolation.current = _State;
			StateInterpolation.target = _State;
			RunChange_State(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Round);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _MaxRounds);
			if ((0x4 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _RoundTime);
			if ((0x8 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _RoundTimer);
			if ((0x10 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _BuyTime);
			if ((0x20 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _PostRoundTime);
			if ((0x40 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _DefuseTime);
			if ((0x80 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _BombPlanted);
			if ((0x1 & _dirtyFields[1]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _State);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (RoundInterpolation.Enabled)
				{
					RoundInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					RoundInterpolation.Timestep = timestep;
				}
				else
				{
					_Round = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_Round(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (MaxRoundsInterpolation.Enabled)
				{
					MaxRoundsInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					MaxRoundsInterpolation.Timestep = timestep;
				}
				else
				{
					_MaxRounds = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_MaxRounds(timestep);
				}
			}
			if ((0x4 & readDirtyFlags[0]) != 0)
			{
				if (RoundTimeInterpolation.Enabled)
				{
					RoundTimeInterpolation.target = UnityObjectMapper.Instance.Map<float>(data);
					RoundTimeInterpolation.Timestep = timestep;
				}
				else
				{
					_RoundTime = UnityObjectMapper.Instance.Map<float>(data);
					RunChange_RoundTime(timestep);
				}
			}
			if ((0x8 & readDirtyFlags[0]) != 0)
			{
				if (RoundTimerInterpolation.Enabled)
				{
					RoundTimerInterpolation.target = UnityObjectMapper.Instance.Map<float>(data);
					RoundTimerInterpolation.Timestep = timestep;
				}
				else
				{
					_RoundTimer = UnityObjectMapper.Instance.Map<float>(data);
					RunChange_RoundTimer(timestep);
				}
			}
			if ((0x10 & readDirtyFlags[0]) != 0)
			{
				if (BuyTimeInterpolation.Enabled)
				{
					BuyTimeInterpolation.target = UnityObjectMapper.Instance.Map<float>(data);
					BuyTimeInterpolation.Timestep = timestep;
				}
				else
				{
					_BuyTime = UnityObjectMapper.Instance.Map<float>(data);
					RunChange_BuyTime(timestep);
				}
			}
			if ((0x20 & readDirtyFlags[0]) != 0)
			{
				if (PostRoundTimeInterpolation.Enabled)
				{
					PostRoundTimeInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					PostRoundTimeInterpolation.Timestep = timestep;
				}
				else
				{
					_PostRoundTime = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_PostRoundTime(timestep);
				}
			}
			if ((0x40 & readDirtyFlags[0]) != 0)
			{
				if (DefuseTimeInterpolation.Enabled)
				{
					DefuseTimeInterpolation.target = UnityObjectMapper.Instance.Map<float>(data);
					DefuseTimeInterpolation.Timestep = timestep;
				}
				else
				{
					_DefuseTime = UnityObjectMapper.Instance.Map<float>(data);
					RunChange_DefuseTime(timestep);
				}
			}
			if ((0x80 & readDirtyFlags[0]) != 0)
			{
				if (BombPlantedInterpolation.Enabled)
				{
					BombPlantedInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					BombPlantedInterpolation.Timestep = timestep;
				}
				else
				{
					_BombPlanted = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_BombPlanted(timestep);
				}
			}
			if ((0x1 & readDirtyFlags[1]) != 0)
			{
				if (StateInterpolation.Enabled)
				{
					StateInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					StateInterpolation.Timestep = timestep;
				}
				else
				{
					_State = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_State(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (RoundInterpolation.Enabled && !RoundInterpolation.current.UnityNear(RoundInterpolation.target, 0.0015f))
			{
				_Round = (int)RoundInterpolation.Interpolate();
				//RunChange_Round(RoundInterpolation.Timestep);
			}
			if (MaxRoundsInterpolation.Enabled && !MaxRoundsInterpolation.current.UnityNear(MaxRoundsInterpolation.target, 0.0015f))
			{
				_MaxRounds = (int)MaxRoundsInterpolation.Interpolate();
				//RunChange_MaxRounds(MaxRoundsInterpolation.Timestep);
			}
			if (RoundTimeInterpolation.Enabled && !RoundTimeInterpolation.current.UnityNear(RoundTimeInterpolation.target, 0.0015f))
			{
				_RoundTime = (float)RoundTimeInterpolation.Interpolate();
				//RunChange_RoundTime(RoundTimeInterpolation.Timestep);
			}
			if (RoundTimerInterpolation.Enabled && !RoundTimerInterpolation.current.UnityNear(RoundTimerInterpolation.target, 0.0015f))
			{
				_RoundTimer = (float)RoundTimerInterpolation.Interpolate();
				//RunChange_RoundTimer(RoundTimerInterpolation.Timestep);
			}
			if (BuyTimeInterpolation.Enabled && !BuyTimeInterpolation.current.UnityNear(BuyTimeInterpolation.target, 0.0015f))
			{
				_BuyTime = (float)BuyTimeInterpolation.Interpolate();
				//RunChange_BuyTime(BuyTimeInterpolation.Timestep);
			}
			if (PostRoundTimeInterpolation.Enabled && !PostRoundTimeInterpolation.current.UnityNear(PostRoundTimeInterpolation.target, 0.0015f))
			{
				_PostRoundTime = (byte)PostRoundTimeInterpolation.Interpolate();
				//RunChange_PostRoundTime(PostRoundTimeInterpolation.Timestep);
			}
			if (DefuseTimeInterpolation.Enabled && !DefuseTimeInterpolation.current.UnityNear(DefuseTimeInterpolation.target, 0.0015f))
			{
				_DefuseTime = (float)DefuseTimeInterpolation.Interpolate();
				//RunChange_DefuseTime(DefuseTimeInterpolation.Timestep);
			}
			if (BombPlantedInterpolation.Enabled && !BombPlantedInterpolation.current.UnityNear(BombPlantedInterpolation.target, 0.0015f))
			{
				_BombPlanted = (bool)BombPlantedInterpolation.Interpolate();
				//RunChange_BombPlanted(BombPlantedInterpolation.Timestep);
			}
			if (StateInterpolation.Enabled && !StateInterpolation.current.UnityNear(StateInterpolation.target, 0.0015f))
			{
				_State = (int)StateInterpolation.Interpolate();
				//RunChange_State(StateInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[2];

		}

		public GameManagerNetworkObject() : base() { Initialize(); }
		public GameManagerNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public GameManagerNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
