﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private int activeItem;

    public GameObject primaryParent;
    public GameObject secondaryParent;

    private WeaponController primaryWeapon;
    private WeaponController secondaryWeapon;

    // Start is called before the first frame update
    void Start()
    {
        activeItem = 0;
        secondaryWeapon = secondaryParent.GetComponentsInChildren<WeaponController>()[0];
        primaryWeapon = primaryParent.GetComponentsInChildren<WeaponController>()[0];
        secondaryWeapon.StoreWeapon();
        primaryWeapon.HoldWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetActiveItem(int item)
    {
        if (activeItem == item)
        {
            return;
        }
        else
        {
            activeItem = item;
            if (item == 0)
            {
                secondaryWeapon.StoreWeapon();
                primaryWeapon.HoldWeapon();
            }
            else
            {
                secondaryWeapon.HoldWeapon();
                primaryWeapon.StoreWeapon();
            }
        }
    }

    public WeaponController GetActiveItem()
    {
        if (activeItem == 0)
        {
            return primaryWeapon;
        }
        else
        {
            return secondaryWeapon;
        }
    }

    public int Switch()
    {
        if (activeItem == 0)
        {
            activeItem = 1;
            secondaryWeapon.HoldWeapon();
            primaryWeapon.StoreWeapon();
        }
        else
        {
            activeItem = 0;
            secondaryWeapon.StoreWeapon();
            primaryWeapon.HoldWeapon();
        }

        return activeItem;
    }

}
