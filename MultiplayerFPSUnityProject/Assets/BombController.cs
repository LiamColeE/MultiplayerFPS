﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine.UI;

public class BombController : BombBehavior
{
    public static BombController instance;

    public override void DefuseBomb(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }

    public override void PlantBomb(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    public void PickupBomb()
    {
        networkObject.TakeOwnership();
    }

    public void DropBomb()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (networkObject.IsOwner)
        {
            if (Input.GetKey(KeyCode.E))
            {
                //plantbomb
            }
        }
    }
}
