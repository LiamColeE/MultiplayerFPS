﻿using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private void Start()
    {
        NetworkManager.Instance.Networker.playerConnected += PlayerConnected;
        NetworkManager.Instance.Networker.playerDisconnected += PlayerDisconnected;

        if (NetworkManager.Instance.IsServer)
        {
            NetworkManager.Instance.InstantiateGameManager();
            if (!Application.isBatchMode)
            {
                NetworkManager.Instance.InstantiatePlayer();
            }
        }
        else
        {
            NetworkManager.Instance.InstantiatePlayer();
        }
    }

    private void PlayerConnected(NetworkingPlayer player, NetWorker sender)
    {
        if (NetworkManager.Instance.IsServer)
        {
            
            Debug.Log("Client Connected to server");
            Debug.Log(NetworkManager.Instance.Networker.Players.Count);
            if (NetworkManager.Instance.Networker.Players.Count >= 4)
            {
                StartGame();
            }
        }
        else
        {
            Debug.Log("Another Client Connected to server");

        }
    }

    private void StartGame()
    {
        Debug.Log("Game Started");
    }

    private void PlayerDisconnected(NetworkingPlayer player, NetWorker sender)
    {
        if (NetworkManager.Instance.IsServer)
        {
            Debug.Log(NetworkManager.Instance.Networker.Players.Count);
            player.Networker.IterateNetworkObjects((NetworkObject networkObject) => { if (networkObject.Owner == player) { networkObject.Destroy(); } });
        }
        Debug.Log("player Disconnected");
        //do something
    }
}
