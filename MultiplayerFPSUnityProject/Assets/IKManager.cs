﻿using DitzelGames.FastIK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKManager : MonoBehaviour
{
    public Inventory inventory;
    public FastIKFabric rightHand;
    public FastIKFabric leftHand;
    // Start is called before the first frame update
    void Start()
    {
        rightHand.Target = inventory.GetActiveItem().rightHand;
        leftHand.Target = inventory.GetActiveItem().leftHand;
    }

    // Update is called once per frame
    void Update()
    {
        rightHand.Target = inventory.GetActiveItem().rightHand;
        leftHand.Target = inventory.GetActiveItem().leftHand;
    }
}
