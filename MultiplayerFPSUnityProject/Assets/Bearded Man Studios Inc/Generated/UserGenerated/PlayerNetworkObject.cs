using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0.15,0.15,0.15,0,0,0,0,0]")]
	public partial class PlayerNetworkObject : NetworkObject
	{
		public const int IDENTITY = 8;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector3 _position;
		public event FieldEvent<Vector3> positionChanged;
		public InterpolateVector3 positionInterpolation = new InterpolateVector3() { LerpT = 0.15f, Enabled = true };
		public Vector3 position
		{
			get { return _position; }
			set
			{
				// Don't do anything if the value is the same
				if (_position == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_position = value;
				hasDirtyFields = true;
			}
		}

		public void SetpositionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_position(ulong timestep)
		{
			if (positionChanged != null) positionChanged(_position, timestep);
			if (fieldAltered != null) fieldAltered("position", _position, timestep);
		}
		[ForgeGeneratedField]
		private Quaternion _rotation;
		public event FieldEvent<Quaternion> rotationChanged;
		public InterpolateQuaternion rotationInterpolation = new InterpolateQuaternion() { LerpT = 0.15f, Enabled = true };
		public Quaternion rotation
		{
			get { return _rotation; }
			set
			{
				// Don't do anything if the value is the same
				if (_rotation == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_rotation = value;
				hasDirtyFields = true;
			}
		}

		public void SetrotationDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_rotation(ulong timestep)
		{
			if (rotationChanged != null) rotationChanged(_rotation, timestep);
			if (fieldAltered != null) fieldAltered("rotation", _rotation, timestep);
		}
		[ForgeGeneratedField]
		private Quaternion _lookRotation;
		public event FieldEvent<Quaternion> lookRotationChanged;
		public InterpolateQuaternion lookRotationInterpolation = new InterpolateQuaternion() { LerpT = 0.15f, Enabled = true };
		public Quaternion lookRotation
		{
			get { return _lookRotation; }
			set
			{
				// Don't do anything if the value is the same
				if (_lookRotation == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x4;
				_lookRotation = value;
				hasDirtyFields = true;
			}
		}

		public void SetlookRotationDirty()
		{
			_dirtyFields[0] |= 0x4;
			hasDirtyFields = true;
		}

		private void RunChange_lookRotation(ulong timestep)
		{
			if (lookRotationChanged != null) lookRotationChanged(_lookRotation, timestep);
			if (fieldAltered != null) fieldAltered("lookRotation", _lookRotation, timestep);
		}
		[ForgeGeneratedField]
		private bool _sprinting;
		public event FieldEvent<bool> sprintingChanged;
		public Interpolated<bool> sprintingInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool sprinting
		{
			get { return _sprinting; }
			set
			{
				// Don't do anything if the value is the same
				if (_sprinting == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x8;
				_sprinting = value;
				hasDirtyFields = true;
			}
		}

		public void SetsprintingDirty()
		{
			_dirtyFields[0] |= 0x8;
			hasDirtyFields = true;
		}

		private void RunChange_sprinting(ulong timestep)
		{
			if (sprintingChanged != null) sprintingChanged(_sprinting, timestep);
			if (fieldAltered != null) fieldAltered("sprinting", _sprinting, timestep);
		}
		[ForgeGeneratedField]
		private float _health;
		public event FieldEvent<float> healthChanged;
		public InterpolateFloat healthInterpolation = new InterpolateFloat() { LerpT = 0f, Enabled = false };
		public float health
		{
			get { return _health; }
			set
			{
				// Don't do anything if the value is the same
				if (_health == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x10;
				_health = value;
				hasDirtyFields = true;
			}
		}

		public void SethealthDirty()
		{
			_dirtyFields[0] |= 0x10;
			hasDirtyFields = true;
		}

		private void RunChange_health(ulong timestep)
		{
			if (healthChanged != null) healthChanged(_health, timestep);
			if (fieldAltered != null) fieldAltered("health", _health, timestep);
		}
		[ForgeGeneratedField]
		private int _activeItem;
		public event FieldEvent<int> activeItemChanged;
		public Interpolated<int> activeItemInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int activeItem
		{
			get { return _activeItem; }
			set
			{
				// Don't do anything if the value is the same
				if (_activeItem == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x20;
				_activeItem = value;
				hasDirtyFields = true;
			}
		}

		public void SetactiveItemDirty()
		{
			_dirtyFields[0] |= 0x20;
			hasDirtyFields = true;
		}

		private void RunChange_activeItem(ulong timestep)
		{
			if (activeItemChanged != null) activeItemChanged(_activeItem, timestep);
			if (fieldAltered != null) fieldAltered("activeItem", _activeItem, timestep);
		}
		[ForgeGeneratedField]
		private bool _ads;
		public event FieldEvent<bool> adsChanged;
		public Interpolated<bool> adsInterpolation = new Interpolated<bool>() { LerpT = 0f, Enabled = false };
		public bool ads
		{
			get { return _ads; }
			set
			{
				// Don't do anything if the value is the same
				if (_ads == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x40;
				_ads = value;
				hasDirtyFields = true;
			}
		}

		public void SetadsDirty()
		{
			_dirtyFields[0] |= 0x40;
			hasDirtyFields = true;
		}

		private void RunChange_ads(ulong timestep)
		{
			if (adsChanged != null) adsChanged(_ads, timestep);
			if (fieldAltered != null) fieldAltered("ads", _ads, timestep);
		}
		[ForgeGeneratedField]
		private int _Team;
		public event FieldEvent<int> TeamChanged;
		public Interpolated<int> TeamInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int Team
		{
			get { return _Team; }
			set
			{
				// Don't do anything if the value is the same
				if (_Team == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x80;
				_Team = value;
				hasDirtyFields = true;
			}
		}

		public void SetTeamDirty()
		{
			_dirtyFields[0] |= 0x80;
			hasDirtyFields = true;
		}

		private void RunChange_Team(ulong timestep)
		{
			if (TeamChanged != null) TeamChanged(_Team, timestep);
			if (fieldAltered != null) fieldAltered("Team", _Team, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			positionInterpolation.current = positionInterpolation.target;
			rotationInterpolation.current = rotationInterpolation.target;
			lookRotationInterpolation.current = lookRotationInterpolation.target;
			sprintingInterpolation.current = sprintingInterpolation.target;
			healthInterpolation.current = healthInterpolation.target;
			activeItemInterpolation.current = activeItemInterpolation.target;
			adsInterpolation.current = adsInterpolation.target;
			TeamInterpolation.current = TeamInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _position);
			UnityObjectMapper.Instance.MapBytes(data, _rotation);
			UnityObjectMapper.Instance.MapBytes(data, _lookRotation);
			UnityObjectMapper.Instance.MapBytes(data, _sprinting);
			UnityObjectMapper.Instance.MapBytes(data, _health);
			UnityObjectMapper.Instance.MapBytes(data, _activeItem);
			UnityObjectMapper.Instance.MapBytes(data, _ads);
			UnityObjectMapper.Instance.MapBytes(data, _Team);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_position = UnityObjectMapper.Instance.Map<Vector3>(payload);
			positionInterpolation.current = _position;
			positionInterpolation.target = _position;
			RunChange_position(timestep);
			_rotation = UnityObjectMapper.Instance.Map<Quaternion>(payload);
			rotationInterpolation.current = _rotation;
			rotationInterpolation.target = _rotation;
			RunChange_rotation(timestep);
			_lookRotation = UnityObjectMapper.Instance.Map<Quaternion>(payload);
			lookRotationInterpolation.current = _lookRotation;
			lookRotationInterpolation.target = _lookRotation;
			RunChange_lookRotation(timestep);
			_sprinting = UnityObjectMapper.Instance.Map<bool>(payload);
			sprintingInterpolation.current = _sprinting;
			sprintingInterpolation.target = _sprinting;
			RunChange_sprinting(timestep);
			_health = UnityObjectMapper.Instance.Map<float>(payload);
			healthInterpolation.current = _health;
			healthInterpolation.target = _health;
			RunChange_health(timestep);
			_activeItem = UnityObjectMapper.Instance.Map<int>(payload);
			activeItemInterpolation.current = _activeItem;
			activeItemInterpolation.target = _activeItem;
			RunChange_activeItem(timestep);
			_ads = UnityObjectMapper.Instance.Map<bool>(payload);
			adsInterpolation.current = _ads;
			adsInterpolation.target = _ads;
			RunChange_ads(timestep);
			_Team = UnityObjectMapper.Instance.Map<int>(payload);
			TeamInterpolation.current = _Team;
			TeamInterpolation.target = _Team;
			RunChange_Team(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _position);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _rotation);
			if ((0x4 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _lookRotation);
			if ((0x8 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _sprinting);
			if ((0x10 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _health);
			if ((0x20 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _activeItem);
			if ((0x40 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _ads);
			if ((0x80 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Team);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (positionInterpolation.Enabled)
				{
					positionInterpolation.target = UnityObjectMapper.Instance.Map<Vector3>(data);
					positionInterpolation.Timestep = timestep;
				}
				else
				{
					_position = UnityObjectMapper.Instance.Map<Vector3>(data);
					RunChange_position(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (rotationInterpolation.Enabled)
				{
					rotationInterpolation.target = UnityObjectMapper.Instance.Map<Quaternion>(data);
					rotationInterpolation.Timestep = timestep;
				}
				else
				{
					_rotation = UnityObjectMapper.Instance.Map<Quaternion>(data);
					RunChange_rotation(timestep);
				}
			}
			if ((0x4 & readDirtyFlags[0]) != 0)
			{
				if (lookRotationInterpolation.Enabled)
				{
					lookRotationInterpolation.target = UnityObjectMapper.Instance.Map<Quaternion>(data);
					lookRotationInterpolation.Timestep = timestep;
				}
				else
				{
					_lookRotation = UnityObjectMapper.Instance.Map<Quaternion>(data);
					RunChange_lookRotation(timestep);
				}
			}
			if ((0x8 & readDirtyFlags[0]) != 0)
			{
				if (sprintingInterpolation.Enabled)
				{
					sprintingInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					sprintingInterpolation.Timestep = timestep;
				}
				else
				{
					_sprinting = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_sprinting(timestep);
				}
			}
			if ((0x10 & readDirtyFlags[0]) != 0)
			{
				if (healthInterpolation.Enabled)
				{
					healthInterpolation.target = UnityObjectMapper.Instance.Map<float>(data);
					healthInterpolation.Timestep = timestep;
				}
				else
				{
					_health = UnityObjectMapper.Instance.Map<float>(data);
					RunChange_health(timestep);
				}
			}
			if ((0x20 & readDirtyFlags[0]) != 0)
			{
				if (activeItemInterpolation.Enabled)
				{
					activeItemInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					activeItemInterpolation.Timestep = timestep;
				}
				else
				{
					_activeItem = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_activeItem(timestep);
				}
			}
			if ((0x40 & readDirtyFlags[0]) != 0)
			{
				if (adsInterpolation.Enabled)
				{
					adsInterpolation.target = UnityObjectMapper.Instance.Map<bool>(data);
					adsInterpolation.Timestep = timestep;
				}
				else
				{
					_ads = UnityObjectMapper.Instance.Map<bool>(data);
					RunChange_ads(timestep);
				}
			}
			if ((0x80 & readDirtyFlags[0]) != 0)
			{
				if (TeamInterpolation.Enabled)
				{
					TeamInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					TeamInterpolation.Timestep = timestep;
				}
				else
				{
					_Team = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_Team(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (positionInterpolation.Enabled && !positionInterpolation.current.UnityNear(positionInterpolation.target, 0.0015f))
			{
				_position = (Vector3)positionInterpolation.Interpolate();
				//RunChange_position(positionInterpolation.Timestep);
			}
			if (rotationInterpolation.Enabled && !rotationInterpolation.current.UnityNear(rotationInterpolation.target, 0.0015f))
			{
				_rotation = (Quaternion)rotationInterpolation.Interpolate();
				//RunChange_rotation(rotationInterpolation.Timestep);
			}
			if (lookRotationInterpolation.Enabled && !lookRotationInterpolation.current.UnityNear(lookRotationInterpolation.target, 0.0015f))
			{
				_lookRotation = (Quaternion)lookRotationInterpolation.Interpolate();
				//RunChange_lookRotation(lookRotationInterpolation.Timestep);
			}
			if (sprintingInterpolation.Enabled && !sprintingInterpolation.current.UnityNear(sprintingInterpolation.target, 0.0015f))
			{
				_sprinting = (bool)sprintingInterpolation.Interpolate();
				//RunChange_sprinting(sprintingInterpolation.Timestep);
			}
			if (healthInterpolation.Enabled && !healthInterpolation.current.UnityNear(healthInterpolation.target, 0.0015f))
			{
				_health = (float)healthInterpolation.Interpolate();
				//RunChange_health(healthInterpolation.Timestep);
			}
			if (activeItemInterpolation.Enabled && !activeItemInterpolation.current.UnityNear(activeItemInterpolation.target, 0.0015f))
			{
				_activeItem = (int)activeItemInterpolation.Interpolate();
				//RunChange_activeItem(activeItemInterpolation.Timestep);
			}
			if (adsInterpolation.Enabled && !adsInterpolation.current.UnityNear(adsInterpolation.target, 0.0015f))
			{
				_ads = (bool)adsInterpolation.Interpolate();
				//RunChange_ads(adsInterpolation.Timestep);
			}
			if (TeamInterpolation.Enabled && !TeamInterpolation.current.UnityNear(TeamInterpolation.target, 0.0015f))
			{
				_Team = (int)TeamInterpolation.Interpolate();
				//RunChange_Team(TeamInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PlayerNetworkObject() : base() { Initialize(); }
		public PlayerNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PlayerNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
