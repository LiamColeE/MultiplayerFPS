﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking;
using UnityEngine.Rendering.PostProcessing;

public class PlayerController : PlayerBehavior
{
    [SerializeField]
    public GameManager.Team team = GameManager.Team.None;

    public Inventory inventory;

    public float defaultSpeed;
    public float jumpHeight;
    public float gravity = -9.81f;
    public CharacterController controller;
    public float groundDistance;
    public LayerMask groundMask;

    public float MouseSense = 1f;
    public Transform camera;
    float xRot = 0f;

    Vector3 velocity;
    bool isGrounded;

    private float currentXVelocity;
    private float currentZVelocity;

    public float health; 

    public Vector2 PlayerVelocity
    {
        get
        {
            return new Vector2(currentXVelocity, currentZVelocity);
        }
    }

    private void Start()
    {   
        if (networkObject.IsOwner)
        {
            Cursor.lockState = CursorLockMode.Locked;
            networkObject.health = 100;
            gameObject.tag = "LocalPlayer";
        }
        else
        {
            Destroy(camera.gameObject.GetComponent<PostProcessLayer>());
            Destroy(camera.gameObject.GetComponent<Camera>());
            Destroy(camera.gameObject.GetComponent<AudioListener>());
        }
    }


    // Update is called once per frame
    void Update()
    {
        health = networkObject.health;
        if (networkObject.IsOwner)
        {

            if (team == GameManager.Team.None)
            {
                Debug.Log(networkObject.MyPlayerId);
                team = GameManager.Instance.GetPlayerTeam(networkObject.MyPlayerId);
                networkObject.Team = (int)team;
            }

            //Handle movement and looking
            isGrounded = Physics.CheckSphere(new Vector3(this.transform.position.x, this.transform.position.y - .6f, this.transform.position.z), groundDistance, groundMask);

            float speed = defaultSpeed;

            if (isGrounded)
            {
                if (velocity.y < 0)
                {
                    velocity.y = -2f;
                }

                if (Input.GetButton("Jump"))
                {
                    velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
                }

                if (Input.GetKey(KeyCode.LeftShift))
                {
                    speed = defaultSpeed * 2f;
                }

                float x = Input.GetAxis("Horizontal");
                float z = Input.GetAxis("Vertical");

                currentXVelocity = x;
                currentZVelocity = z;

                Vector3 move = transform.right * x + transform.forward * z;
                controller.Move(move * speed * Time.deltaTime);
            }
            else
            {
                Vector3 move = transform.right * currentXVelocity + transform.forward * currentZVelocity;
                controller.Move(move * speed * Time.deltaTime);
            }

            velocity.y += gravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);

            float mouseX = Input.GetAxis("Mouse X") * 100 * MouseSense * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * 100 * MouseSense * Time.deltaTime;

            xRot -= mouseY;
            xRot = Mathf.Clamp(xRot, -90f, 90f);

            camera.localRotation = Quaternion.Euler(xRot, 0f, 0f);

            this.transform.Rotate(Vector3.up * mouseX);

            networkObject.position = this.transform.position;
            networkObject.rotation = this.transform.rotation;
            networkObject.lookRotation = camera.rotation;

            //handle inventory
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                networkObject.activeItem = inventory.Switch();
            }

            //handle shooting
            if (inventory.GetActiveItem().fullAuto)
            {
                if (Input.GetAxis("Fire") > 0)
                {
                    networkObject.SendRpc(RPC_SHOOT, Receivers.All);
                }
            }
            else
            {
                if (Input.GetButtonDown("Fire"))
                {
                    networkObject.SendRpc(RPC_SHOOT, Receivers.All);
                }
            }

            //handle aiming
            if (Input.GetButtonDown("ADS")) {
                Debug.Log("ADS");
                inventory.GetActiveItem().ads = true;
                networkObject.ads = true;
            }
            else if (Input.GetButtonUp("ADS"))
            {
                Debug.Log("HIP");
                inventory.GetActiveItem().ads = false;
                networkObject.ads = false;
            }
        }
        else
        {
            this.transform.position = networkObject.position;
            this.transform.rotation = networkObject.rotation;
            camera.rotation = networkObject.lookRotation;
            inventory.SetActiveItem(networkObject.activeItem);
            inventory.GetActiveItem().ads = networkObject.ads;
            team = (GameManager.Team)networkObject.Team;
        }
    }

    public override void Shoot(RpcArgs args)
    {
        if (inventory.GetActiveItem().timeTillNextShot <= 0)
        {
            inventory.GetActiveItem().timeTillNextShot = 1 / inventory.GetActiveItem().firerate;

            if (networkObject.IsServer)
            {
                //hit detection
                int layerMask = 1 << 2;
                layerMask = ~layerMask;

                RaycastHit hit;
                Physics.Raycast(inventory.GetActiveItem().fireLocation.position, inventory.GetActiveItem().fireLocation.forward, out hit, inventory.GetActiveItem().maxDistance, layerMask);

                if (hit.transform != null)
                {
                    PlayerController playerController;
                    hit.transform.gameObject.TryGetComponent<PlayerController>(out playerController);

                    if (playerController != null)
                    {
                        Debug.Log("Hit player");
                        playerController.networkObject.health -= inventory.GetActiveItem().baseDamage;

                        if (networkObject.health <= 0)
                        {
                            Debug.Log("Player Killed");
                        }
                    }
                    else
                    {
                        Debug.Log("didnt player");
                    }
                }
                //visual fire
                inventory.GetActiveItem().Fire();
            }
            else
            {
                Debug.Log("client side");
                inventory.GetActiveItem().Fire();
            }
        }
    }
}
