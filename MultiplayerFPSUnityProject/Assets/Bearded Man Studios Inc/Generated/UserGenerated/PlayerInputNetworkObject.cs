using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0,0,0,0,0,0]")]
	public partial class PlayerInputNetworkObject : NetworkObject
	{
		public const int IDENTITY = 7;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private byte _Forward;
		public event FieldEvent<byte> ForwardChanged;
		public Interpolated<byte> ForwardInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte Forward
		{
			get { return _Forward; }
			set
			{
				// Don't do anything if the value is the same
				if (_Forward == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_Forward = value;
				hasDirtyFields = true;
			}
		}

		public void SetForwardDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_Forward(ulong timestep)
		{
			if (ForwardChanged != null) ForwardChanged(_Forward, timestep);
			if (fieldAltered != null) fieldAltered("Forward", _Forward, timestep);
		}
		[ForgeGeneratedField]
		private byte _Back;
		public event FieldEvent<byte> BackChanged;
		public Interpolated<byte> BackInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte Back
		{
			get { return _Back; }
			set
			{
				// Don't do anything if the value is the same
				if (_Back == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_Back = value;
				hasDirtyFields = true;
			}
		}

		public void SetBackDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_Back(ulong timestep)
		{
			if (BackChanged != null) BackChanged(_Back, timestep);
			if (fieldAltered != null) fieldAltered("Back", _Back, timestep);
		}
		[ForgeGeneratedField]
		private byte _Left;
		public event FieldEvent<byte> LeftChanged;
		public Interpolated<byte> LeftInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte Left
		{
			get { return _Left; }
			set
			{
				// Don't do anything if the value is the same
				if (_Left == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x4;
				_Left = value;
				hasDirtyFields = true;
			}
		}

		public void SetLeftDirty()
		{
			_dirtyFields[0] |= 0x4;
			hasDirtyFields = true;
		}

		private void RunChange_Left(ulong timestep)
		{
			if (LeftChanged != null) LeftChanged(_Left, timestep);
			if (fieldAltered != null) fieldAltered("Left", _Left, timestep);
		}
		[ForgeGeneratedField]
		private byte _Right;
		public event FieldEvent<byte> RightChanged;
		public Interpolated<byte> RightInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte Right
		{
			get { return _Right; }
			set
			{
				// Don't do anything if the value is the same
				if (_Right == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x8;
				_Right = value;
				hasDirtyFields = true;
			}
		}

		public void SetRightDirty()
		{
			_dirtyFields[0] |= 0x8;
			hasDirtyFields = true;
		}

		private void RunChange_Right(ulong timestep)
		{
			if (RightChanged != null) RightChanged(_Right, timestep);
			if (fieldAltered != null) fieldAltered("Right", _Right, timestep);
		}
		[ForgeGeneratedField]
		private byte _Aim;
		public event FieldEvent<byte> AimChanged;
		public Interpolated<byte> AimInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte Aim
		{
			get { return _Aim; }
			set
			{
				// Don't do anything if the value is the same
				if (_Aim == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x10;
				_Aim = value;
				hasDirtyFields = true;
			}
		}

		public void SetAimDirty()
		{
			_dirtyFields[0] |= 0x10;
			hasDirtyFields = true;
		}

		private void RunChange_Aim(ulong timestep)
		{
			if (AimChanged != null) AimChanged(_Aim, timestep);
			if (fieldAltered != null) fieldAltered("Aim", _Aim, timestep);
		}
		[ForgeGeneratedField]
		private byte _Fire;
		public event FieldEvent<byte> FireChanged;
		public Interpolated<byte> FireInterpolation = new Interpolated<byte>() { LerpT = 0f, Enabled = false };
		public byte Fire
		{
			get { return _Fire; }
			set
			{
				// Don't do anything if the value is the same
				if (_Fire == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x20;
				_Fire = value;
				hasDirtyFields = true;
			}
		}

		public void SetFireDirty()
		{
			_dirtyFields[0] |= 0x20;
			hasDirtyFields = true;
		}

		private void RunChange_Fire(ulong timestep)
		{
			if (FireChanged != null) FireChanged(_Fire, timestep);
			if (fieldAltered != null) fieldAltered("Fire", _Fire, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			ForwardInterpolation.current = ForwardInterpolation.target;
			BackInterpolation.current = BackInterpolation.target;
			LeftInterpolation.current = LeftInterpolation.target;
			RightInterpolation.current = RightInterpolation.target;
			AimInterpolation.current = AimInterpolation.target;
			FireInterpolation.current = FireInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _Forward);
			UnityObjectMapper.Instance.MapBytes(data, _Back);
			UnityObjectMapper.Instance.MapBytes(data, _Left);
			UnityObjectMapper.Instance.MapBytes(data, _Right);
			UnityObjectMapper.Instance.MapBytes(data, _Aim);
			UnityObjectMapper.Instance.MapBytes(data, _Fire);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_Forward = UnityObjectMapper.Instance.Map<byte>(payload);
			ForwardInterpolation.current = _Forward;
			ForwardInterpolation.target = _Forward;
			RunChange_Forward(timestep);
			_Back = UnityObjectMapper.Instance.Map<byte>(payload);
			BackInterpolation.current = _Back;
			BackInterpolation.target = _Back;
			RunChange_Back(timestep);
			_Left = UnityObjectMapper.Instance.Map<byte>(payload);
			LeftInterpolation.current = _Left;
			LeftInterpolation.target = _Left;
			RunChange_Left(timestep);
			_Right = UnityObjectMapper.Instance.Map<byte>(payload);
			RightInterpolation.current = _Right;
			RightInterpolation.target = _Right;
			RunChange_Right(timestep);
			_Aim = UnityObjectMapper.Instance.Map<byte>(payload);
			AimInterpolation.current = _Aim;
			AimInterpolation.target = _Aim;
			RunChange_Aim(timestep);
			_Fire = UnityObjectMapper.Instance.Map<byte>(payload);
			FireInterpolation.current = _Fire;
			FireInterpolation.target = _Fire;
			RunChange_Fire(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Forward);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Back);
			if ((0x4 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Left);
			if ((0x8 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Right);
			if ((0x10 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Aim);
			if ((0x20 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _Fire);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (ForwardInterpolation.Enabled)
				{
					ForwardInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					ForwardInterpolation.Timestep = timestep;
				}
				else
				{
					_Forward = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_Forward(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (BackInterpolation.Enabled)
				{
					BackInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					BackInterpolation.Timestep = timestep;
				}
				else
				{
					_Back = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_Back(timestep);
				}
			}
			if ((0x4 & readDirtyFlags[0]) != 0)
			{
				if (LeftInterpolation.Enabled)
				{
					LeftInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					LeftInterpolation.Timestep = timestep;
				}
				else
				{
					_Left = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_Left(timestep);
				}
			}
			if ((0x8 & readDirtyFlags[0]) != 0)
			{
				if (RightInterpolation.Enabled)
				{
					RightInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					RightInterpolation.Timestep = timestep;
				}
				else
				{
					_Right = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_Right(timestep);
				}
			}
			if ((0x10 & readDirtyFlags[0]) != 0)
			{
				if (AimInterpolation.Enabled)
				{
					AimInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					AimInterpolation.Timestep = timestep;
				}
				else
				{
					_Aim = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_Aim(timestep);
				}
			}
			if ((0x20 & readDirtyFlags[0]) != 0)
			{
				if (FireInterpolation.Enabled)
				{
					FireInterpolation.target = UnityObjectMapper.Instance.Map<byte>(data);
					FireInterpolation.Timestep = timestep;
				}
				else
				{
					_Fire = UnityObjectMapper.Instance.Map<byte>(data);
					RunChange_Fire(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (ForwardInterpolation.Enabled && !ForwardInterpolation.current.UnityNear(ForwardInterpolation.target, 0.0015f))
			{
				_Forward = (byte)ForwardInterpolation.Interpolate();
				//RunChange_Forward(ForwardInterpolation.Timestep);
			}
			if (BackInterpolation.Enabled && !BackInterpolation.current.UnityNear(BackInterpolation.target, 0.0015f))
			{
				_Back = (byte)BackInterpolation.Interpolate();
				//RunChange_Back(BackInterpolation.Timestep);
			}
			if (LeftInterpolation.Enabled && !LeftInterpolation.current.UnityNear(LeftInterpolation.target, 0.0015f))
			{
				_Left = (byte)LeftInterpolation.Interpolate();
				//RunChange_Left(LeftInterpolation.Timestep);
			}
			if (RightInterpolation.Enabled && !RightInterpolation.current.UnityNear(RightInterpolation.target, 0.0015f))
			{
				_Right = (byte)RightInterpolation.Interpolate();
				//RunChange_Right(RightInterpolation.Timestep);
			}
			if (AimInterpolation.Enabled && !AimInterpolation.current.UnityNear(AimInterpolation.target, 0.0015f))
			{
				_Aim = (byte)AimInterpolation.Interpolate();
				//RunChange_Aim(AimInterpolation.Timestep);
			}
			if (FireInterpolation.Enabled && !FireInterpolation.current.UnityNear(FireInterpolation.target, 0.0015f))
			{
				_Fire = (byte)FireInterpolation.Interpolate();
				//RunChange_Fire(FireInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PlayerInputNetworkObject() : base() { Initialize(); }
		public PlayerInputNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PlayerInputNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
