﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public Camera camera;
    public bool ads;
    public int totalAmmo;
    public int ammoInClip;
    public bool fullAuto;
    public float firerate;
    public int ADSFOV;
    public Vector3 adsLocation;
    public Vector3 normalLocation;
    public Vector3 storageLocation;
    public int baseDamage;
    public int shotCount;
    public int adsSpeed;
    public float maxDistance;

    public ParticleSystem muzzelFlash;
    public ParticleSystem cartridgeEjection;
    public GameObject impactEffect;

    public Transform fireLocation;

    public float timeTillNextShot;

    private bool isActiveWeapon;

    public Transform rightHand;
    public Transform leftHand;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    public void Fire()
    {
        FireSequence();
    }

    void Update()
    {
        if (timeTillNextShot > 0)
        {
            timeTillNextShot -= Time.deltaTime;
        }
        if (isActiveWeapon)
        {
            if (ads)
            {
                this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, adsLocation, adsSpeed * Time.deltaTime);
                Debug.Log("ADS");
                if (camera != null)
                {
                    Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, ADSFOV, adsSpeed * Time.deltaTime);
                }
            }
            else
            {
                this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, normalLocation, adsSpeed * Time.deltaTime);
                if (camera != null)
                {
                    Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 60, adsSpeed * Time.deltaTime);
                }
            }
        }
    }

    public void StoreWeapon()
    {
        isActiveWeapon = false;
        this.transform.localPosition = storageLocation;
        this.transform.localEulerAngles = new Vector3(90, this.transform.localEulerAngles.y, this.transform.localEulerAngles.z);
    }

    public void HoldWeapon()
    {
        isActiveWeapon = true;
        this.transform.localPosition = normalLocation;
        this.transform.localEulerAngles = new Vector3(0,this.transform.localEulerAngles.y, this.transform.localEulerAngles.z);
    }

    void FireSequence()
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = fireLocation.position;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.SetColors(Color.yellow, Color.yellow);
        lr.SetWidth(0.01f, 0.01f);
        lr.SetPosition(0, fireLocation.position);
        lr.SetPosition(1, fireLocation.position + (fireLocation.forward * 100f));
        GameObject.Destroy(myLine, .02f);
    }
}
